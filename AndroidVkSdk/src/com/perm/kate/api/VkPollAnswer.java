package com.perm.kate.api;

import java.io.Serializable;

public class VkPollAnswer implements Serializable {
    public long id;
    public int votes;
    public String text;
    public int rate;
}
